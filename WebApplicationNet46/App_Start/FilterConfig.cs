﻿using System.Web;
using System.Web.Mvc;

namespace WebApplicationNet46
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
